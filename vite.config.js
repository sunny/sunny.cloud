import { globSync } from 'glob'
import path from 'path'
import { defineConfig } from 'vite'
import { ViteMinifyPlugin } from 'vite-plugin-minify'
import { fileURLToPath } from 'url'
// import { viteSingleFile } from 'vite-plugin-singlefile'

// https://vitejs.dev/config/
export default defineConfig({
  appType: 'mpa',
  build: {
    emptyOutDir: true,
    outDir: '../dist',
    rollupOptions: {
      input: Object.fromEntries(
        globSync('src/**/*.html').map((file) => [
          // This remove `src/` as well as the file extension from each
          // file, so e.g. src/nested/foo.js becomes nested/foo
          path.relative(
            'src',
            file.slice(0, file.length - path.extname(file).length),
          ),
          // This expands the relative paths to absolute paths, so e.g.
          // src/nested/foo becomes /project/src/nested/foo.js
          fileURLToPath(new URL(file, import.meta.url)),
        ]),
      ),
    },
  },
  plugins: [
    // viteSingleFile({
    //   inlinePattern: ['assets/*.css'],
    // }),
    ViteMinifyPlugin(),
  ],
  root: 'src',
  server: {
    port: 3000,
  },
})
